<!DOCTYPE html>
<!-- 
	Author: Anu Dhawan
	Date Created : 21-04-2015	
-->	

<html lang="en">
<head>
<?php
$servername = "localhost";
$username = "root";
$password ="";

//Connect and select a database
mysql_connect($servername, $username, $password);
mysql_select_db("prob2");

$result = mysql_query("SELECT * FROM client_data");
$cost_per_transaction = 0.50;
$cost_per_transaction_above_50 = 0.75;
?>
</head>
<body>
<h1>Problem 2</h1>
<table cellpadding = "2" cellspacing = "3">
	<tr>
		<th>Client Name</th>
		<th>Number Of Transactions</th>
		<th>Charge($)</th>
	</tr>
<?php

	while($row = mysql_fetch_array($result)){
		echo "<tr>";
		echo "<td>".$row['client_name']."</td>";
		echo "<td>".$row['number_of_transaction']."</td>";
		$not = $row['number_of_transaction'];
		$total_cost = 0;
		
		//Checks for number of transactions and calculates total cost
		if($not<=50)
		{
			$total_cost = $not * $cost_per_transaction;
		}
		else
		{
			$cost_of_50 = 25;
			$total_cost = ($not-50) * $cost_per_transaction_above_50;
			$total_cost = $total_cost + $cost_of_50;
		}
		
		echo "<td>$ ".$total_cost."</td>";
		echo "</tr>";
	}
?>
</table>
</body>
</html>