#Problem 2 Database
#Author: Anu Dhawan
#Date Created: 21-04-2015

DROP DATABASE IF EXISTS prob2;

CREATE DATABASE prob2;

USE prob2;

DROP TABLE IF EXISTS client_data;
CREATE TABLE client_data(
	client_name VARCHAR(20) PRIMARY KEY,
	number_of_transaction INT
);

INSERT INTO client_data VALUES ('ClientA', 50);
INSERT INTO client_data VALUES ('ClientB', 20);
INSERT INTO client_data VALUES ('ClientC', 70);
INSERT INTO client_data VALUES ('ClientD', 100);
